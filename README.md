# VolumeControl for iOS

A Swift 5 class for programmatically setting iOS system volume, since Apple doesn't provide an API to directly do so.

# Usage

Just add `VolumeControl.swift` to your project and use it like so:

```
// Optional preloading of shared VolumeControl instance.  
VolumeControl.load() 

// Examples
VolumeControl.shared.setVolume(0.5)   // Sets volume to 50%
VolumeControl.shared.maximizeVolume() // Maximizes volume if not already 100%
VolumeControl.shared.minimizeVolume() // Minimizes volume if not already 0%
```

All volume control operations are asynchronous. 

Preloading the shared `VolumeControl` instance (e.g. in `application(_:didFinishLaunchingWithOptions:)`) is recommended as there would otherwise be a delay when setting volume. This is due to the hacky way the class hijacks the system `MPVolumeView` to simulate user touches for controlling volume. 

This should be approved on the App Store since it isn't using any private APIs, as long as you don't use it for nefarious purposes.
